<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Scraping tool</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style type="text/css">
            .home {
                padding-top: 100px;
            }
            .w700 {
                width: 700px;
            }
        </style>
    </head>
    <body class="home">
        <div class="container">

            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Scraping tool</h2>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-md-12 text-center">
                    {{ Form::open(['route' => 'submit_scraping', 'class' => 'form-inline justify-content-center row']) }}
                        {{ Form::text('url', $data['url'], ['placeholder' => 'PLease input your url', 'class' => 'form-control col-sm-7']) }}
                        {{ Form::submit('Get', ['class' => 'btn btn-primary col-sm-1'])}}
                    {{ Form::close() }}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    @error('url')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="col-md-12 text-center">
                    <p>List supported sources</p>
                    <ul class="list-unstyled">
                        @foreach (App\Factory\ScrapingFactory::SOURCES as $key => $source)
                            <li>{{ $key }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>

            @isset($data['info'])
                @if($data['info'] == null)
                    <div class="row">
                        <div class="col-md-12">
                            <p>There is no information on this link</p>
                        </div>
                    </div>
                @else
                    <h3>Thông tin truyện</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <tr>
                                    <th>Tên truyện</th>
                                    <td>{{ $data['info']['name'] }}</td>
                                </tr>
                                <tr>
                                    <th>Tác giả</th>
                                    <td>{{ $data['info']['author'] }}</td>
                                </tr>
                                <tr>
                                    <th>Mô tả</th>
                                    <td>{{ $data['info']['description'] }}</td>
                                </tr>
                            </table>

                            @if($data['info']['exist'])
                                <div class="alert alert-warning">
                                    <p>This novel already existing on system or job queue is executing!!!</p>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
            @endif
        </div>
    </body>
</html>
