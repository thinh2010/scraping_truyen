<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNovelContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novel_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('chapter');
            $table->string('name')->nullable();
            $table->text('content');
            $table->integer('sort');

            // Relationship
            $table->bigInteger('novel_id')->unsigned()->index();
            $table->foreign('novel_id')->references('id')->on('novels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novel_contents');
    }
}
