<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Novel extends Model
{
    protected $fillable = [
        'id',
        'name',
        'description',
        'author',
    ];
}
