<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Goutte;
use App\Factory\ScrapingFactory;
use Illuminate\Support\Facades\Validator;
use App\Jobs\ProcessGetNovelContent;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Homepage
     *
     * @param  Request  $request
     * @return View
     */
    public function index(Request $request)
    {
        $viewData = [];
        $viewData['url'] = null;
        if($request->method() == 'POST'){
            $url = $request->get('url');

            $urlInfo = parse_url($url);

            $validator = Validator::make($request->all(), [
                'url' => 'required|url',
            ]);

            $validator->after(function ($validator) use ($urlInfo){
                if (!array_key_exists($urlInfo['host'], ScrapingFactory::SOURCES)) {
                    $validator->errors()->add('url', 'Currently we dont support this source!');
                }
            });

            if ($validator->fails()) {
                return redirect('/')
                            ->withErrors($validator)
                            ->withInput();
            }

            $scraping = ScrapingFactory::build($url);
            $viewData['url'] = $url;
            $viewData['info'] = $scraping->getInfo();
            $viewData['info']['exist'] = $scraping->isNovelExist();

            if (!$viewData['info']['exist']) {
                $chapter = 1;
                for ($page = 1; $page <= $viewData['info']['lastPage']; $page++) {
                    // Add job queue to scraping and store novel content
                    $listLink = $scraping->getListChapterLink($page);
                    $listLink->each(function($node, $i) use(&$scraping, &$chapter) {

                        $link = $node->children($scraping->getChapterElement())->attr('href');
                        ProcessGetNovelContent::dispatch($scraping, $link, $chapter);
                        
                        $chapter++;
                    });
                }
            }
        }

        return view('home', ['data' => $viewData]);
    }
}