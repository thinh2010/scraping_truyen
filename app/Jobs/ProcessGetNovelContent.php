<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Scraping\AbstractScraping;
use App\NovelContent;

class ProcessGetNovelContent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $scraping;
    private $urlChapter;
    private $chapter;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(AbstractScraping $scraping, $urlChapter, $chapter)
    {
        $this->scraping = $scraping;
        $this->urlChapter = $urlChapter;
        $this->chapter = $chapter;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->scraping->getChapter($this->urlChapter, $this->chapter);
        NovelContent::create($data);
    }
}
