<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NovelContent extends Model
{
    protected $fillable = [
        'id',
        'name',
        'chapter',
        'sort',
        'content',
        'novel_id',
    ];

    public function novel() {
    	return $this->belongsTo('App\Novel');
    }
}
