<?php

namespace App\Helper;

class Helper
{
    public static function getChapterNumberFromLink($url)
    {
        preg_match_all('/chuong-\d+/', $url, $matches);

        // remove 'chuong-' in result and return only the number of chapter
        return substr($matches[0][0], 7);
    }

    public static function getPageNumberFromLink($url)
    {
        preg_match_all('/\d+/', $url, $matches);

        return end($matches[0]);
    }
}