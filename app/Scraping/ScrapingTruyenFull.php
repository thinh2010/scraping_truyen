<?php
 
 namespace App\Scraping;

 use Helper;
 use Goutte;
 use App\Jobs\ProcessGetNovelContent;

 class ScrapingTruyenFull extends AbstractScraping
 {
    public function __construct($url)
    {
        parent::__construct($url);
        $this->chapterElement = 'a';
    }

    public function getName()
    {
        return $this->crawler->filter('h3.title')->text();
    }

    public function getAuthor()
    {
        return $this->crawler->filter('.info a')->text();
    }

    public function getDescription()
    {
        return $this->crawler->filter('.desc-text')->html();
    }

    public function getLastPage()
    {
        $count = $this->crawler->filter('.pagination')->count();
        if ($count > 0) {
            $lastPageLink = $this->crawler->filter('li.page-nav')->previousAll()->children('a')->attr('href');
            return Helper::getPageNumberFromLink($lastPageLink);
        } else {
            return 1;
        }
    }

    public function getPageLink($page) {
        return $this->url . 'trang-' . $page;
    }

    public function getChapterContent()
    {
        return $this->chapterCrawler->filter('.chapter-c')->html();
    }

    public function getChapterName() {
        return $this->chapterCrawler->filter('.chapter-title')->text();
    }

    public function getListChapterLink($page)
    {
        $link = $this->getPageLink($page);

        $c = Goutte::request('GET', $link);
        return $c->filter('ul.list-chapter li');
    }
 }