<?php
 
 namespace App\Scraping;

 use Helper;
 use Goutte;
 use App\Jobs\ProcessGetNovelContent;

 class ScrapingWebTruyen extends AbstractScraping
 {
    public function __construct($url)
    {
        parent::__construct($url);
        $this->pageElement = 'h4 a';
        $this->chapterElement = 'h4 a';
    }

    public function getName()
    {
        return $this->crawler->filter('h1 a')->text();
    }

    public function getAuthor()
    {
        return $this->crawler->filter('div.detail-info ul li')->first()->children('a')->text();
    }

    public function getDescription()
    {
        return $this->crawler->filter('article')->html();
    }

    public function getLastPage()
    {
        $count = $this->crawler->filter('.w3-pagination')->count();
        if ($count > 0) {
            $lastPageLink = $this->crawler->filter('ul.w3-pagination li')->last()->children('a')->attr('href');
            return Helper::getPageNumberFromLink($lastPageLink);
        } else {
            return 1;
        }
    }

    public function getChapterContent()
    {
        return $this->chapterCrawler->filter('#divcontent')->html();
    }

    public function getChapterName() {
        return $this->chapterCrawler->filter('h1 a')->text();
    }

    public function getPageLink($page)
    {
        return $this->url . $page;
    }

    public function getListChapterLink($page)
    {
        $link = $this->getPageLink($page);

        $c = Goutte::request('GET', $link);
        return $c->filter('div#divtab.list-chapter ul.w3-ul li');
    }
 }