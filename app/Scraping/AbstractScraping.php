<?php 

namespace App\Scraping;

use App\Contract\IScraping;
use Goutte;
use App\Novel;

abstract class AbstractScraping implements IScraping
{
    protected $url;
    protected $baseUrl;
    protected $crawler;
    protected $chapterCrawler;
    protected $novel_id;
    protected $chapterElement = '';
    protected $pageElement = '';

    public function __construct($url) 
    {
        if (substr($url, -1) != '/') {
            $url .= '/';
        }

        $this->url = $url;

        $urlInfo = parse_url($url);
        $this->baseUrl = $urlInfo['host'];

        $crawler = Goutte::request('GET', $url);
        $this->crawler = $crawler;
    }

    public function getInfo() 
    {
        try {
            $data = [
                'name' => $this->getName(),
                'author' => $this->getAuthor(),
                'description' => $this->getDescription(),
                'lastPage' => $this->getLastPage(),
                // 'exist' => $this->isNovelExist(),
                // 'exist' => true,
            ];
        } catch(\InvalidArgumentException $e) {
            $data = [];
        }
        return $data;
    }

    public function getChapter($link, $chapter)
    {
        $this->chapterCrawler = Goutte::request('GET', $link);
        try {
            $data = [
                'name' => $this->getChapterName(),
                'sort' => $chapter,
                'chapter' => $chapter,
                'content' => $this->getChapterContent(),
                'novel_id' => $this->novel_id,
            ];
        } catch(\InvalidArgumentException $e) {
            $data = [];
        }
        return $data;
    }

    public function isNovelExist()
    {
        $count = Novel::where('name', $this->getName())->count();

        if ($count > 0) {
            $novel = Novel::where('name', $this->getName())->first();
            $this->novel_id = $novel->id;
            return true;
        } else {
            $novel = Novel::create($this->getInfo());
            $this->novel_id = $novel->id;
            return false;
        }
    }

    public function getChapterElement()
    {
        return $this->chapterElement;
    }

    public function getPageElement()
    {
        return $this->pageElement;
    }

    abstract protected function getName();
    abstract protected function getAuthor();
    abstract protected function getDescription();
    abstract protected function getChapterName();
    abstract protected function getChapterContent();
    abstract protected function getPageLink($page);
    abstract protected function getListChapterLink($page);


}