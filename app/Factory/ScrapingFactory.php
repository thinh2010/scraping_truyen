<?php

namespace App\Factory;

use App\Scraping\ScrapingTruyenFull;

class ScrapingFactory {

    const SOURCES = [
        'truyenfull.vn' => 'App\Scraping\ScrapingTruyenFull',
        'webtruyen.com' => 'App\Scraping\ScrapingWebTruyen',
    ];

    public static function build($url) {
        
        $urlInfo = parse_url($url);
        $type = $urlInfo['host'];

        if (isset(self::SOURCES[$type])) {
            $scraping = self::SOURCES[$type];
        } else {
            throw new \Exception("Invalid scraping type given.");
        }

        if (class_exists($scraping)) {
            return new $scraping($url);
        } else {
            throw new \Exception("Scraping $scraping class is not found.");
        }
    }
}