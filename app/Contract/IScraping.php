<?php

namespace App\Contract;

interface IScraping
{
    public function getInfo();
    public function getChapter($link, $chapter);
}