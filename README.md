# Scraping tool
This tool is used for get novel from truyenfull.vn and webtruyen.com then import to database


## Requirement
- node v10
- docker
- docker-compose

## Installation

`cp .env.example .env`

`docker-compose build`
`docker-compose up -d`
`docker-compose exec app composer install`
`docker-compose exec app php artisan key:generate`


`npm install`
`npm run dev`

`sudo chmod -R 777 storage bootstrap/cache `

`docker-compose exec app php artisan queue:work` : this command to listening job queue and execute 

